class NumberFormatter
  extend ActionView::Helpers::NumberHelper

  class << self
    def to_percentage(number, precision = 0)
      number_to_percentage(number, precision: precision)
    end
  end
end