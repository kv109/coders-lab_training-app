class CreateScorers < ActiveRecord::Migration
  def change
    create_table :scorers do |t|
      t.string :name

      t.timestamps
    end
  end
end
