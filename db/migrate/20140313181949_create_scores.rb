class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.belongs_to :game, index: true
      t.belongs_to :scorer, index: true
      t.integer :minute

      t.timestamps
    end
  end
end
