# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140313182013) do

  create_table "games", force: true do |t|
    t.integer  "home_player_id"
    t.integer  "away_player_id"
    t.integer  "home_score"
    t.integer  "away_score"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "games", ["away_player_id"], name: "index_games_on_away_player_id"
  add_index "games", ["home_player_id"], name: "index_games_on_home_player_id"

  create_table "scorers", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "scores", force: true do |t|
    t.integer  "game_id"
    t.integer  "scorer_id"
    t.integer  "minute"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "scores", ["game_id"], name: "index_scores_on_game_id"
  add_index "scores", ["scorer_id"], name: "index_scores_on_scorer_id"

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
