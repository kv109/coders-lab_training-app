# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

[User, Scorer, Game, Score].map(&:delete_all)

10.times do |n|
  User.create first_name: "John", last_name: "Doe #{n}"
  print "User#{n}|"
end

40.times do |n|
  Scorer.create name: "Scorer #{n}"
  print "Scorer#{n}|"
end

all_scorers = Scorer.all

User.all.each do |home_player|
  print "HP: #{home_player.last_name}|"
  User.all.each do |away_player|
    print "AP: #{away_player.last_name}|"
    unless home_player == away_player
      home_score = rand(5)
      away_score = rand(3)
      game = Game.create home_score: home_score, away_score: away_score, home_player: home_player, away_player: away_player
      (home_score + away_score).size.times do
        Score.create! game: game, scorer: all_scorers.sample
      end
    end
  end
end

Score.all.each do |score|
  score.update_attribute :minute, rand(90)+1
end