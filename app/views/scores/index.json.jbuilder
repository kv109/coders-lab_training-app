json.array!(@scores) do |score|
  json.extract! score, :id, :game_id, :scorer_id, :minute
  json.url score_url(score, format: :json)
end
