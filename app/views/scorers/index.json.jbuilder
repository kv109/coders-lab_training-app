json.array!(@scorers) do |scorer|
  json.extract! scorer, :id, :name
  json.url scorer_url(scorer, format: :json)
end
