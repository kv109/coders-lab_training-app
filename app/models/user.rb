class User < ActiveRecord::Base
  has_many :home_games, foreign_key: :home_player_id, class_name: 'Game'
  has_many :away_games, foreign_key: :away_player_id, class_name: 'Game'
  has_many :home_wins, -> { where('home_score > away_score') }, foreign_key: :home_player_id, class_name: 'Game'
  has_many :away_wins, -> { where('away_score > away_score') }, foreign_key: :away_player_id, class_name: 'Game'
  has_many :home_loses, -> { where('home_score < away_score') }, foreign_key: :home_player_id, class_name: 'Game'
  has_many :away_loses, -> { where('away_score < away_score') }, foreign_key: :away_player_id, class_name: 'Game'
  has_many :home_draws, -> { where('home_score = away_score') }, foreign_key: :home_player_id, class_name: 'Game'
  has_many :away_draws, -> { where('away_score = away_score') }, foreign_key: :away_player_id, class_name: 'Game'

  def from_usa?
    false
  end

  def games
    home_games + away_games
  end

  def wins
    home_wins + away_wins
  end

  def draws
    home_draws + away_draws
  end

  def loses
    home_loses + away_loses
  end

  def score
    wins.count * 2
  end
end
