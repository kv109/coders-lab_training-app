class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_user
    User.where(id: params[:u]).first || User.first
  end

  helper_method :current_user

  def application_params
    params.permit(:u)
  end
end
